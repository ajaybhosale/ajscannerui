FROM phnxditeastuscr.azurecr.io/phnx-php-angular-dit-base:v1

MAINTAINER Ajay Bhosale<ajay.bhosale@silicus.com>

# copy local defualt config file for NGINX
COPY default.conf /etc/nginx/sites-available/default

# Pull Source Code
COPY workspace/dist /var/www/html/workspace

# Laravel required commands
WORKDIR /var/www/html/workspace