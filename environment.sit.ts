export const environment = {
    production: false,
    apiUrl: 'http://phnx-scanner-api-sit.azurewebsites.net/',
    redirectUri: 'http://phnx-scanner-ui-sit.azurewebsites.net/oauth',
    signInUrl: 'http://phnx-scanner-ui-sit.azurewebsites.net/',
    transactionGateway:'[{"id":"1","item":"VCI gateway"},{"id":"2","item":"USAePay"},{"id":"3","item":"Phoenix"}]',
    scannerProtocol:'https://',
    showErrorCode: true,
};
