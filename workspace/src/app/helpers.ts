import * as $ from "jquery";
import * as jwt_decode from 'jwt-decode';
import { environment } from '../environments/environment';
import { DatePipe } from '@angular/common';
import { AbstractControl } from '@angular/forms';

export class Helpers {

    static loadStyles(tag, src) {
        if (Array.isArray(src)) {
            $.each(src, function(k, s) {
                $(tag).append($('<link/>').attr('href', s).attr('rel', 'stylesheet').attr('type', 'text/css'));
            });
        } else {
            $(tag).append($('<link/>').attr('href', src).attr('rel', 'stylesheet').attr('type', 'text/css'));
        }
    }

    static unwrapTag(element) {
        $(element).removeAttr('appunwraptag').unwrap();
    }

	/**
	 * Set title markup
	 * @param title
	 */
    static setTitle(title) {
        $('.m-subheader__title').text(title);
    }

    /**
	 * Set title markup
	 * @param title
	 */
    static getBrowserTitle() {
        return 'VeriCheck Inc.';
    }

	/**
	 * Breadcrumbs markup
	 * @param breadcrumbs
	 */
    static setBreadcrumbs(breadcrumbs) {
        if (breadcrumbs) $('.m-subheader__title').addClass('m-subheader__title--separator');

        let ul = $('.m-subheader__breadcrumbs');

        if ($(ul).length === 0) {
            ul = $('<ul/>').addClass('m-subheader__breadcrumbs m-nav m-nav--inline')
                .append($('<li/>').addClass('m-nav__item')
                    .append($('<a/>').addClass('m-nav__link m-nav__link--icon')
                        .append($('<i/>').addClass('m-nav__link-icon la la-home'))));
        }

        $(ul).find('li:not(:first-child)').remove();
        $.each(breadcrumbs, function(k, v) {
            let li = $('<li/>').addClass('m-nav__item')
                .append($('<a/>').addClass('m-nav__link m-nav__link--icon').attr('routerLink', v.href).attr('title', v.title)
                    .append($('<span/>').addClass('m-nav__link-text').text(v.text)));
            $(ul).append($('<li/>').addClass('m-nav__separator').text('-')).append(li);
        });
        $('.m-subheader .m-stack__item:first-child').append(ul);
    }

    static setLoading(enable) {
        let body = $('body');
        if (enable) {
            $(body).addClass('m-page--loading-non-block')
        } else {
            $(body).removeClass('m-page--loading-non-block')
        }
    }

    static bodyClass(strClass) {
        $('body').attr('class', strClass);
    }

    static redirectToLogin() {
        let siteHostName = $(location).attr('host');
        window.location.href = environment.scannerProtocol + siteHostName + '/signin';
    }

    static decodeToken(token) {
        try {
            return jwt_decode(token);
        } catch (error) {
            this.redirectToLogin();
        }
    }

    static setDate(value) {
        if (value.hasOwnProperty('date')) {
            return value;
        } else if (value != null) {
            let dtArr = value.split('-');
            let date = new Date(dtArr[2], dtArr[0], dtArr[1]);

            return {
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth(),
                    day: date.getDate()
                }
            }
        }
    }

    static formatDate(date): string {
        let datePipe = new DatePipe('en-US');
        if (date === null) {
            return '';
        }
        try {
            date = date.date;
            return datePipe.transform(
                new Date(date.year, (date.month - 1), date.day),
                environment.dateFormat
            );
        } catch (e) {
            return '';
        }
    }

    static noWhitespaceValidator(control: any) {
        let isWhitespace = (control.value || '').trim().length === 0;
        let isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    }

    static zeroValidator(control: any) {
        let value = control.value;
        if (value != '' && value >= 0.01) {
            return null;
        } else {
            return { 'is_less_than': true };
        }
    }

    static endDateValidator(control: AbstractControl) {
        let parent = control.parent;
        if (parent != undefined) {
            let startDate = Helpers.formatDate(parent.controls['from_date'].value).replace(/^\D+/g, '');
            let endDate = Helpers.formatDate(parent.controls['to_date'].value).replace(/^\D+/g, '');
            // let startDate = parent.controls['from_date'].value;
            // let endDate =parent.controls['to_date'].value; 

            if (endDate >= startDate) {
                return null;
            } else {
                return { 'end_date': true };
            }
        }
    }

    static startDateValidator(control: AbstractControl) {
        let parent = control.parent;
        if (parent != undefined) {
            let startDate = Helpers.formatDate(parent.controls['from_date'].value).replace(/^\D+/g, '');
            let endDate = Helpers.formatDate(parent.controls['to_date'].value).replace(/^\D+/g, '');

            //  let startDate = parent.controls['from_date'].value;
            // let endDate =parent.controls['to_date'].value;

            if (endDate >= startDate) {
                return null;
            } else {
                return { 'start_date': true }
            }
        }
    }

    static onlyNumber(evt, acceptDot = false) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (!acceptDot || charCode != 46) {
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        }
        return true;
    }

    static amtValidator(control: any) {
        let value = control.value;
        if (value >= 0.01 && value <= 9999999.99) {
            return null;
        } else {
            return { 'amt_validate': true };
        }
        /*if(value!='' && value >= 0.01){
             return null;
        }else{
           return  { 'is_less_than': true };
        }*/
    }

}