import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { CookieService } from 'ngx-cookie-service';

declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

    userInfo: any;
    actionTab: string;
    isProfile: boolean;
    constructor(private CookieService: CookieService) {
        this.isProfile = true;
        if (this.CookieService.check('token')) {
            let token = this.CookieService.get('token');

            this.userInfo = Helpers.decodeToken(token);
        }
    }
    ngOnInit() {

    }
    ngAfterViewInit() {
        mLayout.initHeader();
    }
}