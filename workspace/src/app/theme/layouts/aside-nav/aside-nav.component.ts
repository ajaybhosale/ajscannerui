import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { Router } from "@angular/router";

declare let mLayout: any;
@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {

    constructor(private router: Router) {

    }
    ngOnInit() {

    }
    ngAfterViewInit() {

        mLayout.initAside();

    }

    users() {
        if (this.router.url === '/users') {
            this.router.navigate(['/users/all']);
        } else {
            this.router.navigate(['/users']);
        }

    }

    refresh(): void {
        history.go(0);
    }

}