import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AuthenticationService } from '../../../../auth/_services/authentication.service';
import { Router } from '@angular/router';

@Component({
    selector: "app-index",
    templateUrl: "./index.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit, AfterViewInit {


    constructor(private _script: ScriptLoaderService, private _auth: AuthenticationService, private router: Router, private titleService: Title) {

    }
    ngOnInit() {
        let Newtitle = Helpers.getBrowserTitle() + ' | Dashboard';
        this.titleService.setTitle(Newtitle);

        this._auth.showHeaderFooter();
    }
    ngAfterViewInit() {
        this._script.loadScripts('app-index',
            ['assets/app/js/dashboard.js']);

    }

}