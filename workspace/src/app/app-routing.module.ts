import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from "./auth/logout/logout.component";

const routes: Routes = [
    { path: 'logout', component: LogoutComponent },
    //{ path: '', "loadChildren": ".\/theme\/pages\/default\/index\/index.module#IndexModule", pathMatch: 'full' }
    {
        path: '',
        redirectTo: '/scanner/scan-data',
        pathMatch: 'full'
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }