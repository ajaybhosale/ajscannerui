import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';
import { Observable } from "rxjs/Rx";
import { Helpers } from "../../helpers";
import { UserService } from "../_services/user.service";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private _router: Router, private _userService: UserService, private cookieService: CookieService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

        if (this.cookieService.check('token')) {

            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            return this._userService.verify().map(
                data => {
                    if (data !== null) {
                        return true;
                    }
                    return true;
                },
                error => {
                    return true;
                });
        }
        else {
            Helpers.setLoading(true);
            Helpers.redirectToLogin();
            return false;
        }

    }
}