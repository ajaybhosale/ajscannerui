import { Injectable } from "@angular/core";
import { NavigationStart, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Subject } from "rxjs/Subject";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Message } from 'primeng/primeng';


export class AlertMessage {
    public show: boolean;
    public message: string;
    public statusCode: number;
}


@Injectable()
export class AlertService {
    public alertStatus: BehaviorSubject<AlertMessage> = new BehaviorSubject<AlertMessage>({ show: false, message: null, statusCode: null });

    showAlert(isShow: boolean, msg: string, code: number) {
        let alertObj: AlertMessage = { show: isShow, message: msg, statusCode: code };
        this.alertStatus.next(alertObj);
    }
}