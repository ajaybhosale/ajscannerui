import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import "rxjs/add/operator/map";
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthenticationService {
    private subject = new Subject<boolean>();
    private parentURL: string = environment.apiUrl;
    private redirectUri: string = environment.redirectUri;
    private userEntity = 'user';

    constructor(private http: Http, private CookieService: CookieService) {
    }
    showHeaderFooter() {
        this.subject.next(true);
    }

    logout() {
        // remove token cookie
        this.CookieService.delete('token');
        this.CookieService.delete('ANSC');
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        sessionStorage.removeItem("scannerSetting");
        sessionStorage.removeItem("scannerUrl");
        // remove permission session storage
    }
}