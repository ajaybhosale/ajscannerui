import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { Helpers } from "./helpers";
import { ScriptLoaderService } from './_services/script-loader.service';
import { Subscription } from "rxjs/Subscription";
import { AuthenticationService } from './auth/_services/authentication.service';
import { SettingsService } from './scanner/settings/settings.service'
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../environments/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
    subscription: Subscription;
    isLogin: boolean;
    title = 'app';
    globalBodyClass = 'm-page--loading-non-block m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';
    constructor(private _script: ScriptLoaderService, private _login: AuthenticationService, private _router: Router, private CookieService: CookieService, private settingsService: SettingsService) {
    }

    ngOnInit() {
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;

        this.settingsService.getSettingsList(merchantId).subscribe(response => {
            if (response[0] != undefined) {
                let scannerDetail = response[0];
                sessionStorage.setItem("scannerDetail", JSON.stringify(scannerDetail));
                let scannerHost = environment.scannerProtocol + scannerDetail.ip + ':' + scannerDetail.port + '/';
                sessionStorage.setItem("scannerUrl", scannerHost);
            } else {
                sessionStorage.removeItem("scannerSetting");
                sessionStorage.removeItem("scannerUrl");
            }
        },
            error => {
                Helpers.setLoading(false);
            });


        this._script.loadScripts('body', ['assets/vendors/base/vendors.bundle.min.js', 'assets/demo/default/base/scripts.bundle.min.js'], true)
            .then(result => {
                Helpers.setLoading(false);
            });

        this._router.events.subscribe((route) => {
            if (route instanceof NavigationStart) {
                Helpers.setLoading(true);
                Helpers.bodyClass(this.globalBodyClass);
                if (window.location.href.includes('/scanner/result') && sessionStorage.getItem("transactionGroupId") == null) { // REDIRECT RESULT PAGE IF NO DATA IN SESSION                   
                    window.location.href = '#/scanner/scan-data';
                }
            }
            if (route instanceof NavigationEnd) {
                Helpers.setLoading(false);
                if (!this._router.url.includes('/scanner/result') && sessionStorage.getItem("transactionGroupId") != null) { // CLEAR SESSION RATHER THAN RESULT PAGE
                    sessionStorage.removeItem("transactionGroupId");
                    sessionStorage.removeItem("transactionData");
                }
            }
            if (route instanceof NavigationError) {
                this._router.navigate(['scanner/404-page']);
            }
        });

    }
}