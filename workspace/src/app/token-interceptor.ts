import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';
import { CookieService } from 'ngx-cookie-service';
import { AlertService } from './auth/_services/alert.service';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private CookieService: CookieService, private alertService: AlertService, private router: Router) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let headers;
        if (req.url.includes(environment.apiUrl)) {
            headers = new HttpHeaders({
                'Authorization': 'Bearer ' + this.CookieService.get('token'),
                'VeriCheck-Scanner-Version': '1.0.0',
                'Access-Control-Allow-Origin': '*'
            });
        } else {
            headers = new HttpHeaders({
            });
        }

        const authReq = req.clone({ headers });

        let dateTime = new Date();
        dateTime.setMinutes(dateTime.getMinutes() + 20);
        let token = this.CookieService.get('token');

        if ('' == token) {
            this.router.navigate(['/logout']);
        }

        this.CookieService.set('token', this.CookieService.get('token'), dateTime);
        this.alertService.showAlert(true, null, null);
        return next.handle(authReq)
            .catch((error, caught) => {
                if (error instanceof HttpErrorResponse) {
                    if (error.status === 403) {
                        this.alertService.showAlert(true, "Permission denied.", error.status);
                    }
                }
                return Observable.throw(error);
            });
    }
}