import { Injectable, ErrorHandler } from "@angular/core";
import { environment } from '../../environments/environment';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    private showErrorCode: boolean = environment.showErrorCode;
    private arrErrors: any = [];

    constructor() {
    }

    handleError(errors: any): void {
        errors.forEach(element => {
            if (this.showErrorCode) {
                this.arrErrors[element.code] = element.message + " (Err Code: " + element.code + ")";
            } else {
                this.arrErrors[element.code] = element.message;
            }
        });

        return this.arrErrors;
    }
}