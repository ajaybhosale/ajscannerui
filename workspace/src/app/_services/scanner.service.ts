import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Helpers } from '../helpers';
import { Router } from '@angular/router';
import { scannerCommand } from './scanner-command';

declare let document: any;

interface Scanner {
    src: string;
    loaded: boolean;
}

@Injectable()
export class ScannerService {
    buicSetParam: BuicSetParam = new BuicSetParam();
    constructor(private _http: HttpClient, private router: Router) {
        let scannerHost = sessionStorage.getItem('scannerUrl');
        this.scannerInitialize(scannerHost);
    }

    scannerInitialize(scannerHost: string) {
        if (scannerHost != null) {
            this.sendScannerCommand(scannerHost, scannerCommand.BUICINIT, "", "").subscribe(response => {
            }, error => {
            });
            let param: any = {};
            param.Param = scannerCommand.CFG_MISC_SCANBATCH_ENABLE;
            param.Value = scannerCommand.BUIC_DEV_OFF;
            //param.Value = scannerCommand.BUIC_DEV_ON;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param, "").subscribe(response => {
            }, error => {
            });
            let param2: any = {};
            param2.Param = scannerCommand.CFG_DCCSCAN_OPTIONS;
            param2.Value = 1 + 2 + 8192 + 1024;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param2, "").subscribe(response => {
            }, error => {
            });
            /*let param3:any = {};
            param3.Param = 12;
            param3.Value = 8;
            this.sendScannerCommand(scannerHost,scannerCommand.BUICSETPARM,param3,"").subscribe(response => {
                },error => {
            });
            let param4:any = {};
            param4.Param = scannerCommand.CFG_DCCSCAN_ENDCONTRAST;
            param4.Value = 500;
            this.sendScannerCommand(scannerHost,scannerCommand.BUICSETPARM,param4,"").subscribe(response => {
                },error => {
            });*/
            let param5: any = {};
            param5.Param = scannerCommand.CFG_MISC_JPEG_QUALITY;
            param5.Value = 25;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param5, "").subscribe(response => {
            }, error => {
            });
            let param6: any = {};
            param6.Param = scannerCommand.CFG_DEV_PRINTER;
            param6.Value = scannerCommand.BUIC_DEV_OFF;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param6, "").subscribe(response => {
            }, error => {
            });
        }
    }

    setScanner(scannerHost) {
        if (scannerHost != null) {
            this.sendScannerCommand(scannerHost, scannerCommand.BUICINIT, "", "").subscribe(response => {
                console.log(response);
            }, error => {
            });
            let param: any = {};
            param.Param = scannerCommand.CFG_MISC_SCANBATCH_ENABLE;
            param.Value = scannerCommand.BUIC_DEV_OFF;
            //param.Value = scannerCommand.BUIC_DEV_ON;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param, "").subscribe(response => {
                console.log(response);
            }, error => {
            });
            let param2: any = {};
            param2.Param = scannerCommand.CFG_DCCSCAN_OPTIONS;
            param2.Value = 1 + 2 + 8192 + 1024;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param2, "").subscribe(response => {
                console.log(response);
            }, error => {
            });
            /*let param3:any = {};
            param3.Param = 12;
            param3.Value = 8;
            this.sendScannerCommand(scannerHost,scannerCommand.BUICSETPARM,param3,"").subscribe(response => {
                },error => {
            });
            let param4:any = {};
            param4.Param = scannerCommand.CFG_DCCSCAN_ENDCONTRAST;
            param4.Value = 500;
            this.sendScannerCommand(scannerHost,scannerCommand.BUICSETPARM,param4,"").subscribe(response => {
                },error => {
            });*/
            let param5: any = {};
            param5.Param = scannerCommand.CFG_MISC_JPEG_QUALITY;
            param5.Value = 25;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param5, "").subscribe(response => {
                console.log(response);
            }, error => {
            });
            let param6: any = {};
            param6.Param = scannerCommand.CFG_DEV_PRINTER;
            param6.Value = scannerCommand.BUIC_DEV_OFF;
            this.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param6, "").subscribe(response => {
                console.log(response);
            }, error => {
            });
        }
    }

    sendScannerCommand(host, command, param, callback): Observable<any> {
        this.buicSetParam.Function = 'DCCCommand';
        this.buicSetParam.FunctionCall = command;
        if (param) {
            this.buicSetParam.Param = param.Param;
            this.buicSetParam.Value = param.Value;
        }
        let body = JSON.stringify(this.buicSetParam);
        return this._http.post(host + 'command/' + command, body).catch(this.handleError).finally(() => {
        });
    }

    private handleError(error: Response) {
        return Observable.throw(error);
    }
}

export class BuicSetParam {
    Function: string;
    FunctionCall: string;
    Param: any;
    Value: any;
}