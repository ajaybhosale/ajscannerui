// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env:prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const scannerCommand = {
    //  Command Values
    BUICINIT: 1,
    BUICEXIT: 2,
    BUICSTATUS: 3,
    BUICSTATUSDELAY: 4,
    BUICGETSCANNERINFO: 5,
    BUICGETSCANNERSERIALNUMBER: 6,
    BUICSETPARM: 7,
    BUICGETPARM: 8,
    BUICEJECTDOCUMENT: 9,
    DCCSCANGETOCRMICR: 10,
    DCCBATCHPRINTSTRING: 11,
    DCCBATCHPRINTBMP: 12,
    DCCSCAN: 13,
    DCCIMAGE: 14,
    BUICCLEARDOCUMENT: 15,
    BUICINITPATH: 16,
    BUICDEBUG: 17,
    BUICSETPARAMSTRING: 18,
    BUICGETPARAMSTRING: 19,
    BUICREADCONFIG: 20,
    BUICWRITECONFIG: 21,
    DCCSCANLONG: 22,
    BUICCOMPRESSIMAGE: 23,
    BUICCOMPRESSIMAGEGRAY: 24,

    FINDBUICSCANNER: 31,/*  Deprecated */
    BUICSCAN: 32,
    BUICSCANMEMORY: 33,
    BUICSCANGRAY: 34,
    BUICSCANMEMORYGRAY: 35,
    BUICFLASHIMAGE: 36,/*  Deprecated */
    BUICROTATEIMAGE: 37,/*  Deprecated */
    BUICSTARTIMAGEWINDOW: 38,/*  Deprecated */
    BUICCLOSEWINDOW: 39,/*  Deprecated */
    BUICSIZEIMAGEWINDOW: 40,/*  Deprecated */
    BUICFORCEIMAGEDISPAY: 41,/*  Deprecated */
    BUICSETIMAGEMULTIPAGE: 42,/*  Deprecated */
    BUICCOPYFILE: 43,
    BUICCOMBINETIFFS: 44,
    BUICCROPFILE: 45,

    BUICSCANVIRTUALENDORSEMENT: 51,
    DCCSCANSETSPECIALDOCUMENT: 52,

    DCCGETSCANNERTYPE: 53,
    DCCGETAPIVERSION: 54,
    DCCGETAPIDATE: 55,
    DCCGETSUPPORTEDSCANNERS: 56,
    ISDCCSCANNERAVAILABLE: 57,

    BUICEJECTPOCKET: 61,
    BUICSETPARMSARRAY: 101,

    //  Generic Values for Parameters
    BUIC_DEV_ON: 1,
    BUIC_DEV_OFF: 0,

    //   Formats
    FORMAT_TIFF_UNCOMP: 0,
    FORMAT_TIFF: 1,
    FORMAT_BMP: 3,
    FORMAT_JPEGG: 4,

    //  On-Off based parameters
    //      All of these are set to BUIC_DEV_OFF/ON  or simply 0 or 1
    CFG_MICR_ENABLE: 1,
    CFG_MISC_SCANBATCH_ENABLE: 160,
    CFG_MISC_FORCE_EJECT: 184,
    CFG_MISC_DEBUG_MESSAGES: 162,
    CFG_DEV_DOUBLE_FEED: 7,
    CFG_MISC_IMAGEWAIT: 109,
    BPARAM_PRINTER: 31,
    CFG_DEV_PRINTER: 31,

    //  MICR setting
    CFG_MICR_FONT: 2,
    //  These are possible settings for teh type of MICR
    BUIC_CMC7: 0,
    BUIC_E13B: 1,
    CFG_MICR_AUTO: 3,

    CFG_MICR_FORMAT: 57,
    MICR_FORMAT_NOSPACE: 0,
    MICR_FORMAT_ALLSPACES: 1,

    //  MICR Verification method
    CFG_MISC_MICR_VERIFY: 116,
    //  Possible settings are 0 - 4

    CFG_FIRMW_LOAD_MODE: 201,
    CFG_FIRMW_LOAD_MODE_NO: 0,   /* No Dowload.  IN-flash firmware is used */
    CFG_FIRMW_LOAD_MODE_ALWAYS: 1,   /* Always download Firmware */
    CFG_FIRMW_LOAD_MODE_DEFAULT: 2,   /* DEFAULT VALUE */
    CFG_FIRMW_LOAD_MODE_NEWER: 2,   /* Download if firmware with greater file  */

    //  Options are a bitmapped integer
    CFG_DCCSCAN_OPTIONS: 156,
    //  Bit wise values...

    //  Sets the quality level for JPEG image compression
    TPARAM_JPEG_QUALITY: 110,
    CFG_MISC_JPEG_QUALITY: 110,
    // The quality variable is  1 - 100

    CFG_IMAGE_BACK_BW_THRESH: 12,   /* Range from 0 to 15 */
    CFG_DCCSCAN_STARTCONTRAST: 147,  //Usually around 450
    CFG_DCCSCAN_ENDCONTRAST: 148  //Usually 450 to 900

};
