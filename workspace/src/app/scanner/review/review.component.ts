import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core'
import { DatePipe } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Helpers } from '../../helpers';
import { CookieService } from 'ngx-cookie-service';
import { ReviewService } from '../review/review.service';
import { environment } from '../../../environments/environment';
import { Settings } from '../settings/settings';
/*import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/mergeMap';
import { Observable } from 'rxjs/Rx';*/
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IMyDpOptions } from 'mydatepicker';

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
declare var jQuery: any;

@Component({
    selector: "app-review",
    templateUrl: "./review.component.html",
    encapsulation: ViewEncapsulation.None
})

export class ReviewComponent implements OnInit {
    reviewForm: FormGroup;
    reviewList: any[] = [];
    reviewListCopy: any[] = [];
    isSuccess: boolean;
    isError: boolean;
    successMsg: string;
    errorMsg: string;
    isFormSubmit: boolean;
    recordObject: any;
    recordIndex: number;
    gridTotalAmount: number;
    back_image: any;
    front_image: any;
    hasPopupMessage: boolean;
    popupMessages: string[] = [];
    invalidDateMessage: string;
    secCodeList: any;
    beforeDisplay: boolean;
    sortBy: string = 'date';
    sortOrderBy: string = 'desc';
    datePlaceHolder: string = environment.dateFormat.toLowerCase();
    amtErrorMsg: string = 'Amount is required';
    myDatePickerOptions: IMyDpOptions = {
        dateFormat: environment.dateFormat.toLowerCase(),
        indicateInvalidDate: true,
        openSelectorTopOfInput: true,
        selectorHeight: '264px',
        selectorWidth: '264px',
        showSelectorArrow: false
    };
    showErrorCode: boolean = environment.showErrorCode;
    fieldCode: any = {
        payee_name: "CM001",
        payor_name: "CM002",
        date: "CM003",
        sec_code: "CM004",
        amount: "CM005",
        account_number: "CM007",
        routing_number: "CM006",
        check_number: "SC008"
    };
    @ViewChild('editForm') editForm: ElementRef;
    @ViewChild('deleteModal') deleteModal: ElementRef;
    @ViewChild('viewModal') viewModal: ElementRef;

    constructor(
        private formBuilder: FormBuilder,
        private CookieService: CookieService,
        private titleService: Title,
        private router: Router,
        private datePipe: DatePipe,
        private reviewService: ReviewService
    ) {

    }

    ngOnInit() {
        this.hideErrors();
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;
        this.isFormSubmit = false;
        this.recordObject = '';
        this.gridTotalAmount = 0;
        this.front_image = '';
        this.back_image = '';
        this.invalidDateMessage = "Date is required";
        this.beforeDisplay = false;
        this.secCodeList = JSON.parse(environment.transactionGateway);
        this.setFormData(undefined);
        this.titleService.setTitle(Helpers.getBrowserTitle() + " | Review");

        Helpers.setLoading(true);
        this.reviewService.getReviewCheckList(merchantId).subscribe(reponse => {
            this.reviewList = [];
            this.reviewListCopy = [];
            this.reviewList = this.reviewFilterList(reponse);

            this.beforeDisplay = true;
            Helpers.setLoading(false);
            this.reviewListCopy = this.reviewList;
        },
            error => {
                Helpers.setLoading(false);
            });
    }

    reviewFilterList(reviewDataList, sortBy = 'reviewDataList.updated_on', isDescending = true) {
        this.gridTotalAmount = 0;
        reviewDataList.filter(reviewItem => {
            reviewItem.account_number = atob(reviewItem.account_number);
            reviewItem.routing_number = atob(reviewItem.routing_number);
            if (reviewItem.amount) {
                this.gridTotalAmount = Number(this.gridTotalAmount) + Number(reviewItem.amount);
            }
        });
        return reviewDataList;
    }

    hideErrors() {
        this.isError = false;
        this.isSuccess = false;
        this.successMsg = "";
        this.errorMsg = "";
        this.hasPopupMessage = false;
        this.popupMessages = [];
    }


    setFormData(reviewItem: any) {
        let regAlpNumericSpace = "^[A-Za-z0-9- ]+$";
        //let regAmount = "^[0-9]+(\.[0-9]{1,2})?$";
        let regAmount = /^([0-9]{1,7})(\.[0-9]{1,2})?$/;
        let regNumeric = "^[0-9]*$";
        if (reviewItem == undefined) {
            this.reviewForm = this.formBuilder.group({
                payee_name: ['', [Validators.required, Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                payor_name: ['', [Validators.required, Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                date: ['', [Validators.required, Validators.maxLength(15)]],
                sec_code: ['', [Validators.required, Validators.maxLength(3)]],
                amount: ['', [Validators.required, Validators.pattern(regAmount), Helpers.amtValidator, Helpers.zeroValidator]],
                check_number: ['', [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]],
                account_number: ['', [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(20)]],
                routing_number: ['', [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]]
            });
        } else {
            let date = Helpers.setDate(reviewItem.date);
            this.reviewForm = this.formBuilder.group({
                payee_name: [reviewItem.payee_name, [Validators.required, Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                payor_name: [reviewItem.payor_name, [Validators.required, Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                date: [date, [Validators.required, Validators.maxLength(15)]],
                sec_code: [reviewItem.sec_code, [Validators.required, Validators.maxLength(3)]],
                amount: [reviewItem.amount, [Validators.required, Validators.pattern(regAmount), Helpers.amtValidator, Helpers.zeroValidator]],
                check_number: [reviewItem.check_number, [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]],
                account_number: [reviewItem.account_number, [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(20)]],
                routing_number: [reviewItem.routing_number, [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]]
            });
        }
    }

    openForm(reviewItem: any, index, sortedData) {
        this.hideErrors();

        this.front_image = '';
        this.back_image = '';

        this.reviewList = sortedData;
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;

        this.recordObject = reviewItem;
        this.recordIndex = index;
        this.setFormData(this.recordObject);

        let imgObj: any = {}
        imgObj.merchant_id = merchantId;
        imgObj.front_image_name = reviewItem.front_image_name
        imgObj.back_image_name = reviewItem.back_image_name

        Helpers.setLoading(true);
        this.reviewService.getImage(imgObj).subscribe(response => {
            this.front_image = response.front_image;
            this.back_image = response.back_image;
            Helpers.setLoading(false);
        }, error => {
            Helpers.setLoading(false);
        });
        jQuery(this.editForm.nativeElement).modal('show');
    }

    onDateChanged(event: any) {
        if (event.formatted != '') {
            this.recordObject.date = event.formatted;
        }

    }

    validateDate(event: any) {
        this.invalidDateMessage = (event.value != '' && !event.valid) ? "Invalid date format" : "Date is required";
    }

    saveForm(chkObject, isvalid) {
        this.isFormSubmit = true;
        this.hideErrors();
        if (isvalid) {
            let token = this.CookieService.get('token');
            let currentUserObj = Helpers.decodeToken(token);
            let merchantId = currentUserObj.oid;
            chkObject.merchant_id = merchantId;
            let index = this.recordIndex;

            Helpers.setLoading(true);
            let transactionSave: any = {};
            transactionSave = this.recordObject;
            transactionSave.payee_name = chkObject.payee_name;
            transactionSave.payor_name = chkObject.payor_name;
            transactionSave.sec_code = chkObject.sec_code;
            //transactionSave.date = chkObject.date;            
            transactionSave.amount = chkObject.amount;
            transactionSave.routing_number = chkObject.routing_number;
            transactionSave.account_number = chkObject.account_number;
            transactionSave.check_number = chkObject.check_number;
            transactionSave.updated_by = merchantId;
            transactionSave.updated_on = this.datePipe.transform(new Date(), environment.dateFormat);
            this.reviewService.saveReviewCheck(transactionSave).subscribe(response => {
                if (response != null) {
                    if (response.isValid) {
                        //this.ngOnInit();
                        this.reviewList.splice(index, 1, response);
                        this.reviewList = this.reviewFilterList(this.reviewList);

                        this.recordObject = {};
                        this.isSuccess = true;
                        this.isError = false;
                        this.errorMsg = "";
                        this.successMsg = "Record updated successfully.";
                        jQuery(this.editForm.nativeElement).modal('hide');
                    } else {
                        this.hasPopupMessage = true
                        this.popupMessages = eval(response.validationError);
                    }
                }
                Helpers.setLoading(false);
            },
                error => {
                    Helpers.setLoading(false);
                });
        }
    }

    closeForm() {
        this.ngOnInit();
        jQuery(this.editForm.nativeElement).modal('hide');
    }

    confirmDelete(reviewItem, index, sortedData) {
        this.reviewList = sortedData;
        this.recordObject = reviewItem;
        this.recordIndex = index;
        this.hideErrors();
        jQuery(this.deleteModal.nativeElement).modal('show');
    }

    deleteData() {
        Helpers.setLoading(true);
        jQuery(this.deleteModal.nativeElement).modal('hide');
        let delObj: any = this.recordObject;
        let index = this.recordIndex;
        //delObj.date = this.ngbDateParserFormatter.format(delObj.date);
        this.reviewService.deleteReviewCheck(delObj).subscribe(response => {
            this.recordObject = {};
            if (response) {
                //this.ngOnInit();                                
                this.reviewList.splice(index, 1);//REMOVE ELEMENT FROM ARRAY
                this.reviewList = this.reviewFilterList(this.reviewList);
                this.isSuccess = true;
                this.isError = false;
                this.errorMsg = "";
                this.successMsg = "Record deleted successfully.";
            } else {
                this.ngOnInit();
                this.isSuccess = false;
                this.isError = true;
                this.errorMsg = "Something went wrong.";
                this.successMsg = "";
            }
            Helpers.setLoading(false);
        },
            error => {
                Helpers.setLoading(false);
            });
    }

    viewForm(reviewItem) {
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;

        this.front_image = '';
        this.back_image = '';

        this.recordObject = reviewItem;
        let imgObj: any = {}
        imgObj.merchant_id = merchantId;
        imgObj.front_image_name = reviewItem.front_image_name
        imgObj.back_image_name = reviewItem.back_image_name

        Helpers.setLoading(true);
        this.reviewService.getImage(imgObj).subscribe(response => {
            this.front_image = response.front_image;
            this.back_image = response.back_image;
            Helpers.setLoading(false);
        });
        this.hideErrors();
        jQuery(this.viewModal.nativeElement).modal('show');
    }

    makeTransaction(reviewList) {
        this.hideErrors();
        if (reviewList.length) {
            Helpers.setLoading(true);
            this.reviewService.makeTransaction(reviewList).subscribe(response => {
                Helpers.setLoading(false);
                if (response != null) {
                    sessionStorage.setItem('transactionData', btoa(JSON.stringify(response)));
                    sessionStorage.setItem('transactionGroupId', btoa(response[0].transaction_group_id));
                }
                this.router.navigate(['/scanner/result']);
            }, error => {
                Helpers.setLoading(false);
            });
        } else {
            this.isError = true;
            this.errorMsg = "There are no records to process.";
        }
    }

    hideMessage() {
        this.hideErrors();
    }

    addDecimal(event) {
        let ctrlValue = event.target.value.trim();
        const control = this.reviewForm.get('amount');

        let regAmount = /^([0-9]{1,7})(\.[0-9]{0,7})?$/;

        if (ctrlValue != '') {
            if (regAmount.test(ctrlValue) && control.valid) {
                event.target.value = parseFloat(ctrlValue).toFixed(2);
                control.setValue(event.target.value);
            }
        }
    }

    amtValidateMessage(event) {
        let ctrlValue = event.target.value.trim();
        let regAmount = /^[0-9]{1,7}$/;
        //let regAmount = /^(?:[0-9]+(?:\.[0-9]{0,2})?)?$/;
        this.amtErrorMsg = 'Amount is required';
        if (ctrlValue == '') {
            this.amtErrorMsg = 'Amount is required';
        } else {
            if (!regAmount.test(ctrlValue)) {
                if (isNaN(ctrlValue)) {
                    this.amtErrorMsg = 'Amount must be in digits (*.00)';
                } else {
                    this.amtErrorMsg = 'Amount should be maximum of 7 digits';
                }
            }
        }
    }

    onlyNumber(evt, acceptDot = false) {
        return Helpers.onlyNumber(evt, acceptDot);
    }
}