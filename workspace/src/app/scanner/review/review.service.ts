import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Helpers } from '../../helpers';

@Injectable()
export class ReviewService {
    private parentURL: string = environment.apiUrl;
    private scanData = 'check';

    constructor(private _http: HttpClient, private router: Router) { }

    getReviewCheckList(merchantId: string) {
        return this._http.get(this.parentURL + this.scanData + "/getChecksForReview?id=" + merchantId).catch(this.handleError).finally(() => {
        });
    }

    saveReviewCheck(chkObj: any) {
        return this._http.post(this.parentURL + this.scanData + "/updateReviewDetails", chkObj).catch(this.handleError).finally(() => {
        });
    }

    deleteReviewCheck(delObj: any) {
        return this._http.post(this.parentURL + this.scanData + "/deleteCheckDetails", delObj).catch(this.handleError).finally(() => {
        });
    }

    getImage(imgObj: any) {
        return this._http.post(this.parentURL + this.scanData + "/getCheckImages", imgObj).catch(this.handleError).finally(() => {
        });
    }

    makeTransaction(makeTransaction) {
        return this._http.post(this.parentURL + "transaction/makeTransaction", makeTransaction).catch(this.handleError).finally(() => {
        });
    }

    private handleError(error: Response) {
        return Observable.throw(error);
    }

}