import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Helpers } from '../../helpers';

@Injectable()
export class ScanDataService {
    private parentURL: string = environment.apiUrl;
    private scanData = 'check';
    private limit = 'limit=10';

    constructor(private _http: HttpClient, private router: Router) { }

    getCheckList(merchantId: string) {
        return this._http.get(this.parentURL + this.scanData + "/getCheck?id=" + merchantId).catch(this.handleError).finally(() => {
        });
    }

    saveScanData(scanData: any) {
        return this._http.post(this.parentURL + this.scanData + "/", scanData).catch(this.handleError).finally(() => {
        });
    }

    saveCheck(chkObj: any) {
        return this._http.put(this.parentURL + this.scanData + "/", chkObj).catch(this.handleError).finally(() => {
        });
    }

    deleteCheck(delObj: any) {
        return this._http.post(this.parentURL + this.scanData + "/deleteCheckDetails", delObj).catch(this.handleError).finally(() => {
        });
    }

    deleteScanCheck(id: string) {
        return this._http.get(this.parentURL + this.scanData + "/updateChecksForReview?merchantId=" + id).catch(this.handleError).finally(() => {
        });
    }

    getSecCodes(companieId: string) {
        return this._http.get(environment.phnxApi + 'companies/' + companieId).catch(this.handleError).finally(() => {
        });
    }

    private handleError(error: Response) {
        return Observable.throw(error);
    }

}