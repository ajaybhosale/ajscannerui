import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { ScanDataService } from '../scan-data/scan-data.service';
import { Transaction } from '../scan-data/scan-data';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Helpers } from '../../helpers';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../environments/environment';
import { Settings } from '../settings/settings';
import { ScannerService } from '../../_services/scanner.service';
import { scannerCommand } from '../../_services/scanner-command';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IMyDpOptions } from 'mydatepicker';
import { OrderPipe } from 'ngx-order-pipe';

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
declare var jQuery: any;

@Component({
    selector: "app-scan",
    templateUrl: "./scan-data.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class ScanDataComponent implements OnInit {

    scanDataForm: FormGroup;
    transactionList: any[] = [];
    scannerObj: any[] = [];
    recordObj: any;
    recordIndex: number;
    isSuccess: boolean;
    isError: boolean;
    successMsg: string;
    errorMsg: string;
    isFormSubmit: boolean[] = [];
    errors: any;
    scannerDetail: Settings;
    count: number;
    bubbleMsgList: string[] = [];
    invalidDateMessage: string[] = [];
    invalidCount: number;
    invalidCheckCount: number;
    checkSavedNumber: number;
    secCodeList: any;
    amtErrorMsg: string;
    datePlaceHolder: string = environment.dateFormat.toLowerCase();
    myDatePickerOptions: IMyDpOptions = {
        dateFormat: environment.dateFormat.toLowerCase(),
        indicateInvalidDate: true,
        openSelectorTopOfInput: true,
        selectorHeight: '264px',
        selectorWidth: '264px',
        showSelectorArrow: false
    };
    showErrorCode: boolean = environment.showErrorCode;
    fieldCode: any = {
        payee_name: "CM001",
        payor_name: "CM002",
        date: "CM003",
        sec_code: "CM004",
        amount: "CM005",
        account_number: "CM007",
        routing_number: "CM006",
        check_number: "SC008"
    };

    @ViewChild('deleteModal') deleteModal: ElementRef;
    @ViewChild('confirmReview') confirmReview: ElementRef;

    constructor(
        private scanDataService: ScanDataService,
        private formBuilder: FormBuilder,
        private CookieService: CookieService,
        private router: Router,
        private scannerService: ScannerService,
        private datePipe: DatePipe,
        private el: ElementRef,
        private orderPipe: OrderPipe,
        private titleService: Title
    ) {

    }

    ngOnInit() {
        this.count = 0;
        this.setScanDataForm(null);
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;
        this.bubbleMsgList = [];
        this.invalidCount = 0;
        this.invalidCheckCount = 0;
        this.checkSavedNumber = 0;
        this.isFormSubmit = [];
        this.titleService.setTitle(Helpers.getBrowserTitle() + " | Scan");

        this.secCodeList = JSON.parse(environment.transactionGateway);
        Helpers.setLoading(true);
        this.scanDataService.getCheckList(merchantId).subscribe(response => {
            Helpers.setLoading(false);
            this.transactionList = [];
            this.transactionList = this.transactionFilterList(response);
            this.setScanDataForm(this.transactionList);
        },
            error => {
                Helpers.setLoading(false);
            });
    }

    transactionFilterList(response, toSort = true) {
        this.bubbleMsgList = [];
        this.invalidCount = 0;
        this.invalidCheckCount = 0;
        this.checkSavedNumber = 0;
        this.isFormSubmit = [];
        let result;

        result = (toSort) ? this.orderPipe.transform(response, 'response.created_on', true) : response;

        if (result != null) {
            result.filter(transaction => {
                transaction.date = (transaction.date != "") ? Helpers.setDate(transaction.date) : "";
                this.isFormSubmit.push(false);
                this.invalidDateMessage.push('Date is required')
                this.bubbleMsgList.push(eval(transaction.validationError));
                if (!transaction.isValid) {
                    let countObj = this.invalidRecordCount(this.bubbleMsgList);
                    this.invalidCheckCount = countObj.invalidCheckCount;
                    this.invalidCount = countObj.totalErrorCount;
                }
                if (environment.scanStatusEdit.includes(transaction.check_status)) {
                    this.checkSavedNumber++;
                }
            });
        }
        return result;
    }

    hideErrors() {
        this.errors = '';
        this.isError = false;
        this.isSuccess = false;
        this.successMsg = "";
        this.errorMsg = "";
    }

    setScanDataForm(transactionList: Transaction[]) {
        // let regAlpNumericSpace = "^[A-Za-z0-9- ]+$";
        //let regAmount = "^[0-9]+(\.[0-9]{1,2})?$";
        let regAmount = /^([0-9]{1,7})(\.[0-9]{1,2})?$/;
        let regNumeric = "^[0-9]*$";
        this.scanDataForm = this.formBuilder.group({
            checkList: this.formBuilder.array([])
        });

        if (transactionList != null) {
            const control = <FormArray>(<FormGroup>this.scanDataForm).controls['checkList'];
            transactionList.filter(transaction => {
                control.push(
                    this.formBuilder.group({
                        payee_name: [transaction.payee_name, [Validators.required, /*Validators.pattern(regAlpNumericSpace),*/ Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                        payor_name: [transaction.payor_name, [Validators.required, /*Validators.pattern(regAlpNumericSpace),*/ Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                        date: [transaction.date, [Validators.required, Validators.maxLength(15)]],
                        sec_code: [transaction.sec_code, [Validators.required, Validators.maxLength(3)]],
                        amount: [transaction.amount, [Validators.required, Validators.pattern(regAmount), Helpers.amtValidator, Helpers.zeroValidator]],
                        check_number: [transaction.check_number, [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]],
                        account_number: [atob(transaction.account_number), [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(20)]],
                        routing_number: [atob(transaction.routing_number), [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]],
                    }));

            });
        } else {
            this.formBuilder.group({
                payee_name: ['', [Validators.required, /*Validators.pattern(regAlpNumericSpace),*/ Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                payor_name: ['', [Validators.required, /*Validators.pattern(regAlpNumericSpace),*/ Validators.maxLength(100), Helpers.noWhitespaceValidator]],
                date: ['', [Validators.required, Validators.maxLength(15)]],
                sec_code: ['', [Validators.required, Validators.maxLength(3)]],
                amount: ['', [Validators.required, Validators.pattern(regAmount), Helpers.amtValidator, Helpers.zeroValidator]],
                check_number: ['', [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]],
                account_number: ['', [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(20)]],
                routing_number: ['', [Validators.required, Validators.pattern(regNumeric), Helpers.zeroValidator, Validators.maxLength(10)]]
            });
        }
    }

    addDecimal(event, index) {
        // let ctrlValue = event.target.value.trim();    
        const control = (<FormArray>(<FormGroup>this.scanDataForm).controls['checkList']);
        const controlGroup = <FormGroup>control.at(index);
        let amtControl = controlGroup.controls['amount'];
        let ctrlValue = controlGroup.controls['amount'].value;
        let regAmount = /^([0-9]{1,7})(\.[0-9]{1,2})?$/;

        if (ctrlValue != '') {
            if (regAmount.test(ctrlValue) && controlGroup.controls['amount'].valid) {
                event.target.value = (parseFloat(ctrlValue)).toFixed(2);
                controlGroup.controls['amount'].setValue(event.target.value);
            }
        }
    }

    amtValidateMessage(event, index) {
        //let ctrlValue = event.target.value.trim();
        const control = (<FormArray>(<FormGroup>this.scanDataForm).controls['checkList']);

        const controlGroup = <FormGroup>control.at(index);
        let ctrlValue = controlGroup.controls['amount'].value;
        let regAmount = /^[0-9]{1,7}$/;
        this.amtErrorMsg = 'Amount is required';

        if (ctrlValue == '') {
            this.amtErrorMsg = 'Amount is required';
        } else {

            if (!regAmount.test(ctrlValue)) {
                if (isNaN(ctrlValue)) {
                    this.amtErrorMsg = 'Amount must be in digits (*.00)';
                } else {
                    if (ctrlValue == null) {
                        this.amtErrorMsg = 'Amount is required';
                    } else {
                        this.amtErrorMsg = 'Amount should be maximum of 7 digits';
                    }
                }
            }
        }
    }

    checkScan(isBatchScan: boolean) {
        this.hideErrors();
        Helpers.setLoading(true);
        let scannerHost = sessionStorage.getItem('scannerUrl');
        this.scannerService.sendScannerCommand(scannerHost, 'BUICSTATUS', '', '').subscribe(response => {
            if (response.Result) {
                if (isBatchScan) {
                    this.batchScan();
                } else {
                    this.singleScan();
                }
            } else {
                this.isError = true;
                this.isSuccess = false;
                this.successMsg = "";
                this.errorMsg = "Please keep check(s) ready for scanning.";
                Helpers.setLoading(false);
            }
        }, error => {
            this.isError = true;
            this.isSuccess = false;
            this.successMsg = "";
            this.errorMsg = "Please check if scanner is connected.";
            Helpers.setLoading(false);
        });
    }

    singleScan() {
        let scannerHost = sessionStorage.getItem('scannerUrl');
        let scannerDetail = JSON.parse(sessionStorage.getItem('scannerDetail'));

        let transactions: Transaction[] = [];
        let transaction: Transaction = new Transaction();

        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;

        let param: any = {};
        param.Param = scannerCommand.CFG_MISC_SCANBATCH_ENABLE;
        param.Value = scannerCommand.BUIC_DEV_OFF;

        let param1: any = {};
        param1.Param = 314; // Track ID is Check
        param1.Value = 0;

        let param2: any = {};
        param2.Param = 6; // Must be 200 DPI
        param2.Value = 1;
        Helpers.setLoading(true);

        this.scannerService.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param, null).subscribe(response => {
            if (response.Result < 0) {
                this.isError = true;
                this.isSuccess = false;
                this.successMsg = "";
                //this.errorMsg = JSON.stringify(response, null, 4);
                this.errorMsg = "Please check scanner is initialize.";
                Helpers.setLoading(false);
            } else {
                this.scannerService.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param1, null).subscribe(response => {

                    if (response.Result < 0) {
                        this.isError = true;
                        this.isSuccess = false;
                        this.successMsg = "";
                        //this.errorMsg = JSON.stringify(response, null, 4);
                        this.errorMsg = "Please check scanner is initialize.";
                        Helpers.setLoading(false);
                    } else {
                        this.scannerService.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param2, null).subscribe(response => {
                            if (response.Result < 0) {
                                this.isError = true;
                                this.isSuccess = false;
                                this.successMsg = "";
                                //this.errorMsg = JSON.stringify(response, null, 4);
                                this.errorMsg = "Please check scanner is initialize.";
                                Helpers.setLoading(false);
                            } else {
                                this.scannerService.sendScannerCommand(scannerHost, scannerCommand.DCCSCAN, null, null).subscribe(response => {
                                     if (response.Result == 0) {
                                        transaction.merchant_id = merchantId;
                                        transaction.merchant_name = currentUserObj.name;
                                        transaction.merchant_location_id = "";
                                        transaction.merchant_location_name = "";
                                        transaction.payee_name = "";
                                        transaction.payor_name = "";
                                        transaction.date = "";
                                        transaction.transaction_gateway = scannerDetail.transaction_gateway;
                                        transaction.serial_number = scannerDetail.serial_no;
                                        transaction.source_key = scannerDetail.source_key;
                                        transaction.check_status = environment.scanStatusScanned;
                                        transaction.created_by = merchantId;
                                        transaction.created_on = this.datePipe.transform(new Date(), environment.dateFormat + " " + environment.timeFormat);
                                        if (response.MicrFields.Fld7 != '') { /*BUSINESS CHECK*/
                                            transaction.check_number = response.MicrFields.Fld7;
                                            transaction.sec_code = "";
                                            transaction.account_number = response.MicrFields.Fld3;
                                            transaction.routing_number = response.MicrFields.Fld5;
                                        } else {
                                            transaction.check_number = response.MicrFields.Fld2;
                                            transaction.sec_code = "";
                                            transaction.account_number = response.MicrFields.Fld3;
                                            transaction.routing_number = response.MicrFields.Fld5;
                                        }
                                        this.convertToByteSingle(scannerHost + "DCCCommand/fgs_" + this.count, this, transaction, false);
                                        this.convertToByteSingle(scannerHost + "DCCCommand/rgs_" + this.count, this, transaction, true);
                                    }else if (response.Result <= -1) {// IF ERROR occurred while scaning, especially for check scaning
                                        Helpers.setLoading(false);
                                    }
                                });
                            }
                        });
                    }
                });
            }

        },
            error => {
                Helpers.setLoading(false);
            });
    }

    convertToByteSingle(url: any, reference, transaction, flag) {

        const promise =
            new Promise((resolve, reject) => {
                var xhr = new XMLHttpRequest();
                var reader = new FileReader();
                xhr.onload = function() {
                    reader.onloadend = function() {
                        let tmp = reader.result;
                        if (flag) {
                            transaction.Back_Image = tmp;
                            resolve(reader.result);
                        } else {
                            transaction.Front_Image = tmp;
                        }
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url, true);
                xhr.responseType = 'blob';
                xhr.send(null);
            })
                .then((data) => {
                    reference.scannerObj.push(transaction);
                    let transactions = [];
                    transactions = this.scannerObj;
                    this.scanDataService.saveScanData(transactions)
                        .subscribe(response => {
                            if (response != null) {
                                this.scannerObj = [];
                                //let res = this.transactionFilterList(response);
                                //this.transactionList.push(response[0]);
                                this.transactionList.unshift(response[0]);
                                this.transactionList = this.transactionFilterList(this.transactionList, false);
                                this.setScanDataForm(this.transactionList);
                                //this.ngOnInit();
                            }
                            Helpers.setLoading(false);

                        }, error => {
                            Helpers.setLoading(false);
                        });
                });
    }

    batchScan() {
        let scannerHost = sessionStorage.getItem('scannerUrl');
        let scannerDetail = JSON.parse(sessionStorage.getItem('scannerDetail'));

        let transactions: Transaction[] = [];
        let transaction: Transaction = new Transaction();

        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;

        let param: any = {};
        param.Param = scannerCommand.CFG_MISC_SCANBATCH_ENABLE;
        param.Value = scannerCommand.BUIC_DEV_ON;

        let param1: any = {};
        param1.Param = 314; // Track ID is Check
        param1.Value = 0;

        let param2: any = {};
        param2.Param = 6; // Must be 200 DPI
        param2.Value = 1;
        Helpers.setLoading(true);
        this.scannerService.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param, null).subscribe(response => {
            this.hideErrors()
            if (response.Result < 0) {
                /*this.isError = true;
                this.isSuccess = false;
                this.successMsg = "";
                //this.errorMsg = JSON.stringify(response, null, 4);
                this.errorMsg = "Please check scanner is initialize.";                
                Helpers.setLoading(false);*/
            }
        });
        this.scannerService.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param1, null).subscribe(response => {
            this.hideErrors()
            if (response.Result < 0) {
                /* this.isError = true;
                 this.isSuccess = false;
                 this.successMsg = "";
                 //this.errorMsg = JSON.stringify(response, null, 4);
                 this.errorMsg = "Please check scanner is initialize.";                
                 Helpers.setLoading(false);*/
            }
        });
        this.scannerService.sendScannerCommand(scannerHost, scannerCommand.BUICSETPARM, param2, null).subscribe(response => {
            this.hideErrors()
            if (response.Result < 0) {
                /* this.isError = true;
                 this.isSuccess = false;
                 this.successMsg = "";
                 //this.errorMsg = JSON.stringify(response, null, 4);
                 this.errorMsg = "Please check scanner is initialize.";                
                 Helpers.setLoading(false);*/
            }
        });
        this.scannerService.sendScannerCommand(scannerHost, scannerCommand.DCCSCAN, null, null).subscribe(response => {
             if (response.Result == 0) {
                transaction.merchant_id = merchantId;
                transaction.merchant_name = currentUserObj.name;
                transaction.merchant_location_id = "";
                transaction.merchant_location_name = "";
                transaction.payee_name = "";
                transaction.payor_name = "";
                transaction.date = "";
                transaction.transaction_gateway = scannerDetail.transaction_gateway;
                transaction.serial_number = scannerDetail.serial_no;
                transaction.source_key = scannerDetail.source_key;
                transaction.check_status = environment.scanStatusScanned;
                transaction.created_by = merchantId;
                transaction.created_on = this.datePipe.transform(new Date(), environment.dateFormat + " " + environment.timeFormat);
                if (response.MicrFields.Fld7 != '') { /*BUSINESS CHECK*/
                    transaction.check_number = response.MicrFields.Fld7;
                    transaction.sec_code = "";
                    transaction.account_number = response.MicrFields.Fld3;
                    transaction.routing_number = response.MicrFields.Fld5;
                } else {
                    transaction.check_number = response.MicrFields.Fld2;
                    transaction.sec_code = "";
                    transaction.account_number = response.MicrFields.Fld3;
                    transaction.routing_number = response.MicrFields.Fld5;
                }

                this.convertToByteBatch(scannerHost + "DCCCommand/fgs_" + this.count, this, transaction, false);
                this.convertToByteBatch(scannerHost + "DCCCommand/rgs_" + this.count, this, transaction, true);                 
            } else {                
                if (response.Result == -212) { // Hopper Empty!
                    transactions = [];
                    transactions = this.scannerObj;
                    this.scanDataService.saveScanData(transactions)
                        .subscribe(response => {
                            if (response != null) {
                                this.scannerObj = [];
                                response.filter(transaction => {
                                    //this.transactionList.push(transaction);
                                    this.transactionList.unshift(transaction);
                                });
                                this.transactionList = this.transactionFilterList(this.transactionList, false);
                                this.setScanDataForm(this.transactionList);
                            }
                            Helpers.setLoading(false);
                        });
                }else if(response.Result <= -1){// IF ERROR occurred while scaning, especially for check scaning 
                    Helpers.setLoading(false);
                }
            }
        },
            error => {
                Helpers.setLoading(false);
            });
    }

    convertToByteBatch(url: any, reference, transaction, flag) {

        let promise =
            new Promise((resolve, reject) => {
                var xhr = new XMLHttpRequest();
                var reader = new FileReader();
                xhr.onload = function() {
                    reader.onloadend = function() {
                        let tmp = reader.result;
                        if (flag) {
                            transaction.Back_Image = tmp;
                            resolve(reader.result);
                        } else {
                            transaction.Front_Image = tmp;
                        }
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url, true);
                xhr.responseType = 'blob';
                xhr.send(null);
            })
                .then((data) => {

                    this.scannerObj.push(transaction);
                    this.count++;
                    //this.transactionList.push(transaction);
                    this.batchScan();
                });
    }

    validateDate(event: any, index) {
        this.invalidDateMessage[index] = (event.value != '' && !event.valid) ? "Invalid date format" : "Date is required";
    }

    onDateChanged(event: any, index) {
        this.transactionList[index].date = event.formatted;
    }

    saveForm(chkObject, arrIndex, isvalid) {
        this.isFormSubmit[arrIndex] = true;
        this.hideErrors();
        if (isvalid) {
            let token = this.CookieService.get('token');
            let currentUserObj = Helpers.decodeToken(token);
            let merchantId = currentUserObj.oid;
            chkObject.merchant_id = merchantId;
            let transactionSave: Transaction;
            transactionSave = this.transactionList[arrIndex];
            transactionSave.payee_name = chkObject.payee_name.trim();
            transactionSave.payor_name = chkObject.payor_name.trim();
            transactionSave.sec_code = chkObject.sec_code.trim();
            if (typeof transactionSave.date == 'object') {
                transactionSave.date = Helpers.formatDate(transactionSave.date);
            } else {
                transactionSave.date = transactionSave.date;
            }
            transactionSave.amount = chkObject.amount.trim();
            transactionSave.routing_number = chkObject.routing_number.trim();
            transactionSave.account_number = chkObject.account_number.trim();
            transactionSave.check_number = chkObject.check_number.trim();
            transactionSave.updated_by = merchantId;
            transactionSave.updated_on = this.datePipe.transform(new Date(), environment.dateFormat + " " + environment.timeFormat);

            Helpers.setLoading(true);
            this.scanDataService.saveCheck(transactionSave).subscribe(response => {
                if (response != null) {
                    if (response.isValid) {
                        this.isSuccess = true;
                        this.isError = false;
                        this.errorMsg = "";
                        this.successMsg = "Record updated successfully.";
                        //response.date = Helpers.setDate(response.date);
                        this.transactionList.splice(arrIndex, 1, response);
                        this.transactionList = this.transactionFilterList(this.transactionList, false);
                        this.setScanDataForm(this.transactionList);
                        // this.ngOnInit();
                    } else {
                        this.isSuccess = false;
                        this.isError = true;
                        this.errorMsg = "Error occurred while saving, please check the errors.";
                        this.successMsg = "";
                        this.transactionList[arrIndex] = response;
                        this.bubbleMsgList[arrIndex] = eval(response.validationError);
                        let countObj = this.invalidRecordCount(this.bubbleMsgList);
                        this.invalidCheckCount = countObj.invalidCheckCount;
                        this.invalidCount = countObj.totalErrorCount;
                        Helpers.setLoading(false);
                    }
                }
                Helpers.setLoading(false);
            },
                error => {
                    Helpers.setLoading(false);
                });
        }
    }

    invalidRecordCount(bubbleMsgList) {
        let totalErrorCount = 0;
        let invalidCheckCount = 0;
        jQuery.each(bubbleMsgList, function(index, value) {
            if (jQuery(this).length) {
                invalidCheckCount++;
            }
            totalErrorCount = totalErrorCount + jQuery(this).length;
        });

        let invalidObj: any = {};
        invalidObj.invalidCheckCount = invalidCheckCount;
        invalidObj.totalErrorCount = totalErrorCount;
        return invalidObj;
    }

    autoClickTooltip() {
        setTimeout(() => {
            var event = document.createEvent("Event");
            event.initEvent("click", false, true);
            this.el
                .nativeElement
                .querySelector('.pull-left')
                .dispatchEvent(event);
        }, 200);
    }

    confirmDelete(recordObj: any, index) {
        this.recordObj = recordObj;
        this.recordIndex = index;
        this.hideErrors();
        jQuery(this.deleteModal.nativeElement).modal('show');
    }

    deleteData() {
        Helpers.setLoading(true);
        jQuery(this.deleteModal.nativeElement).modal('hide');
        let delObj: any = this.recordObj;
        let index = this.recordIndex;
        if (typeof delObj.date == 'object') {// send formated date from object
            delObj.date = Helpers.formatDate(delObj.date);
        }
        this.scanDataService.deleteCheck(delObj).subscribe(response => {
            if (response = "success") {
                //this.ngOnInit();
                this.transactionList.splice(index, 1);//REMOVE ELEMENT FROM ARRAY
                this.transactionList = this.transactionFilterList(this.transactionList, false);
                this.setScanDataForm(this.transactionList);

                this.isSuccess = true;
                this.isError = false;
                this.errorMsg = "";
                this.successMsg = "Record deleted successfully.";
            } else {
                this.ngOnInit();
                this.isSuccess = false;
                this.isError = true;
                this.errorMsg = "Something went wrong.";
                this.successMsg = "";
            }
            Helpers.setLoading(false);
        },
            error => {
                Helpers.setLoading(false);
            });
    }

    confirmReviewBox() {
        this.hideErrors();
        jQuery(this.confirmReview.nativeElement).modal('show');
    }

    deleteScanRecords() {
        jQuery(this.confirmReview.nativeElement).modal('hide');
        this.hideErrors();
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let id = currentUserObj.oid;
        Helpers.setLoading(true);
        this.scanDataService.deleteScanCheck(id).subscribe(response => {
            if (response == "success") {
                Helpers.setLoading(false);
                this.router.navigate(['/scanner/review']);
            } else {
                Helpers.setLoading(false);
                this.ngOnInit();
                this.isSuccess = false;
                this.isError = true;
                this.errorMsg = "Something went wrong.";
                this.successMsg = "";
            }

        },
            error => {
                Helpers.setLoading(false);
            });
    }

    ejectDocument() {
        let scannerHost = sessionStorage.getItem('scannerUrl');
        Helpers.setLoading(true);
        this.scannerService.sendScannerCommand(scannerHost, scannerCommand.BUICEJECTDOCUMENT, null, null).subscribe(response => {
            Helpers.setLoading(false);
        },
            error => {
                Helpers.setLoading(false);
            });
    }

    onlyNumber(evt, acceptDot = false) {
        return Helpers.onlyNumber(evt, acceptDot);
    }
}