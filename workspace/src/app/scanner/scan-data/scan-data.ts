export class Transaction {
    merchant_id: string;
    merchant_name: string;
    merchant_location_id: string;
    merchant_location_name: string;
    payee_name: string;
    payor_name: string;
    date: string;
    sec_code: string;
    account_number: string;
    routing_number: string;
    transaction_gateway: string;
    serial_number: string;
    source_key: string;
    Front_Image: any[] = [];
    Back_Image: any[] = [];
    check_number: string;
    check_status: string;
    created_by: string;
    created_on: string;
    amount: string;
    updated_by: string;
    updated_on: string;
}