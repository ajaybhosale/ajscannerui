import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LayoutModule } from './../theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScannerComponent } from './scanner.component';
import { ScanDataComponent } from './scan-data/scan-data.component';
import { ReviewComponent } from './review/review.component';
import { ResultComponent } from './result/result.component';
import { ReportComponent } from './report/report.component';
import { SettingsComponent } from './settings/settings.component';
import { FormSettingComponent } from './settings/form/form-setting.component';
import { ScannerRoutingModule } from './scanner-routing.module';
import { ScanDataService } from './scan-data/scan-data.service';
import { ReviewService } from './review/review.service';
import { ResultService } from './result/result.service';
import { SettingsService } from './settings/settings.service';
import { ReportService } from './report/report.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MyDatePickerModule } from 'mydatepicker';
import { OrderModule } from 'ngx-order-pipe';
import { DataTableModule } from "angular2-datatable";
import { PageComponent } from './404-page/page.component';

@NgModule({
    declarations: [
        ScannerComponent,
        ScanDataComponent,
        SettingsComponent,
        ResultComponent,
        FormSettingComponent,
        ReviewComponent,
        ReportComponent,
        PageComponent
    ],
    imports: [
        LayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        ScannerRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MyDatePickerModule,
        OrderModule,
        DataTableModule,
        NgbModule.forRoot()
    ],
    providers: [
        ScanDataService,
        SettingsService,
        ReviewService,
        ResultService,
        ReportService
    ],
    bootstrap: []
})
export class ScannerModule { }