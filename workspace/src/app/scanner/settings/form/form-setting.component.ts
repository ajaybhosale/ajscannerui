import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { SettingsService } from '../../settings/settings.service';
import { Settings } from '../../settings/settings';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Helpers } from '../../../helpers';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '../../../../environments/environment';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { ScannerService } from '../../../_services/scanner.service';
import { scannerCommand } from '../../../_services/scanner-command';

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
declare var jQuery: any;

@Component({
    selector: "app-setting-form",
    templateUrl: "./form-setting.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class FormSettingComponent implements OnInit {
    scannerSettingForm: FormGroup;
    merchantId: string;
    isloderTrue: boolean;
    pageNumber: number = 1;
    searchBy: any;
    sortGroupBy: any;
    settingList: Settings[] = [];
    currentRecord: number = 0;
    settingDetails: Settings;
    noOfRecords: number;
    isSuccess: boolean;
    isError: boolean;
    responseMsg: string;
    popupMessage: string;
    isErrorOnMessage: boolean;
    errorOfMessage: string;
    isFormSubmit: boolean;
    messageSuccess: boolean;
    totalItems: number;
    errors: any;
    transcactionGatewayList: any[] = [];
    showErrorCode: boolean = environment.showErrorCode;
    gatewayPinUSA: string = environment.gatewayPinUSA;
    fieldCode: any = {
        serial_no: "SM001",
        ip: "SM002",
        source_key: "SM003",
        transaction_gateway: "SM004",
        port: "SM005",
        pin: "SM006"
    };

    /*@ViewChild('addGroupModal') addGroupModal: ElementRef;
    @ViewChild('confirmModal') confirmModal: ElementRef;
    @ViewChild('search') search: ElementRef;*/

    constructor(
        private settingService: SettingsService,
        private scanerService: ScannerService,
        private formBuilder: FormBuilder,
        private CookieService: CookieService,
        private titleService: Title,
        private router: Router,
        private activatedroute: ActivatedRoute
    ) {

    }

    ngOnInit() {

        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        this.merchantId = currentUserObj.oid;
        this.titleService.setTitle(Helpers.getBrowserTitle() + " | Scanner Settings");

        this.transcactionGatewayList = JSON.parse(environment.transactionGateway);
        this.setScannerSettingForm(undefined);
        Helpers.setLoading(true);
        this.settingService.getSettingsList(this.merchantId).subscribe(response => {
            if (response != "") {
                this.settingDetails = <Settings>response[0];
                this.setScannerSettingForm(this.settingDetails);
                this.showpin();
            }
            Helpers.setLoading(false);
        });

        this.hideErrors();
    }

    setScannerSettingForm(settingData: Settings) {
        let ipPattern = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
        let alphaNumericPattern = "^$|^[A-Za-z0-9]+";
        let digit = "^$|^[0-9]+";
        if (settingData == undefined) {
            this.scannerSettingForm = this.formBuilder.group({
                id: ["", []],
                merchant_id: [this.merchantId, [Validators.required]],
                serial_no: ["", []],
                source_key: ["", [Validators.required, Helpers.noWhitespaceValidator, Validators.maxLength(20)]],
                ip: ["", [Validators.required, Validators.pattern(ipPattern), Validators.minLength(7), Validators.maxLength(15)]],
                port: ["", [Validators.required, Validators.pattern(digit), Validators.minLength(2), Validators.maxLength(4)]],
                transaction_gateway: ["", [Validators.required]],
                pin: [""]
            });
        } else {
            this.scannerSettingForm = this.formBuilder.group({
                id: [settingData.id, []],
                merchant_id: [settingData.merchant_id, [Validators.required]],
                serial_no: [settingData.serial_no, [Validators.required]],
                source_key: [settingData.source_key, [Validators.required, Helpers.noWhitespaceValidator, Validators.maxLength(20)]],
                ip: [settingData.ip, [Validators.required, Validators.pattern(ipPattern), Validators.minLength(7), Validators.maxLength(15)]],
                port: [settingData.port, [Validators.required, Validators.pattern(digit), Validators.minLength(2), Validators.maxLength(4)]],
                transaction_gateway: [settingData.transaction_gateway, [Validators.required]],
                pin: [settingData.pin]
            });
        }
    }

    hideErrors() {
        this.errors = '';
        this.isErrorOnMessage = false;
        this.isError = false;
        this.isSuccess = false;
    }

    saveForm(formData: any, isValid: boolean) {

        this.hideErrors();
        this.isFormSubmit = true;
        let settingsObj = new Settings();
        settingsObj = formData;
        Helpers.setLoading(true);
        if (isValid) {
            let scannerHost = environment.scannerProtocol + settingsObj.ip + ':' + settingsObj.port + '/';
            this.scanerService.scannerInitialize(scannerHost);
            this.scanerService.sendScannerCommand(scannerHost, scannerCommand.BUICGETSCANNERSERIALNUMBER, '', '').subscribe(
                response => {
                    settingsObj.serial_no = response.Serial;
                    if (settingsObj.serial_no != '') {
                        if (settingsObj.id != '') {
                            settingsObj.updated_by = this.merchantId;
                        } else {
                            settingsObj.created_by = this.merchantId;
                            settingsObj.updated_by = this.merchantId;
                        }
                        settingsObj.pin = (settingsObj.transaction_gateway == this.gatewayPinUSA) ? settingsObj.pin : '';
                        this.settingService.addForm(settingsObj)
                            .subscribe(
                            responseData => {
                                if (responseData.hasOwnProperty('errors')) {
                                } else {
                                    this.settingService.getSettingsList(this.merchantId).subscribe(response => {
                                        if (response[0] != undefined) {
                                            let scannerDetail = response[0];
                                            sessionStorage.setItem("scannerDetail", JSON.stringify(scannerDetail));
                                            let scannerHost = environment.scannerProtocol + scannerDetail.ip + ':' + scannerDetail.port + '/';
                                            sessionStorage.setItem("scannerUrl", scannerHost);
                                        } else {
                                            sessionStorage.removeItem("scannerSetting");
                                            sessionStorage.removeItem("scannerUrl");
                                        }
                                    });
                                    this.isSuccess = true;
                                    this.responseMsg = (settingsObj.id == '') ? "Scanner saved successfully." : "Scanner updated successfully.";
                                    this.isFormSubmit = false;
                                    this.router.navigate(['/scanner/settings']);
                                }
                            }, errorError => {
                                this.isErrorOnMessage = true;
                                this.errorOfMessage = 'Unable to process your request, Please contact Administrator.';
                                Helpers.setLoading(false);
                            });
                    } else {
                        this.isErrorOnMessage = true;
                        this.isSuccess = false;
                        this.errorOfMessage = "Scanner is not available.";
                        Helpers.setLoading(false);
                    }

                }, error => {
                    this.isErrorOnMessage = true;
                    this.errorOfMessage = "Scanner is not available. Possible reasons are below:<br/> 1. Incorrect IP / Port. <br/> 2. Scanner is not connected.";
                    Helpers.setLoading(false);

                }
            );
        } else {
            this.errors = '';
            Helpers.setLoading(false);
        }
    }

    onCancel() {
        this.hideErrors();
        this.isFormSubmit = false;
        this.router.navigate(['/scanner/settings']);
    }

    showpin() {
        debugger;
        if (this.scannerSettingForm.controls['pin'].value != undefined) {
            let value = this.scannerSettingForm.controls['transaction_gateway'].value;
            if (value == this.gatewayPinUSA) {
                this.scannerSettingForm.controls['pin'].setValidators([Validators.required, Helpers.noWhitespaceValidator]);
                this.scannerSettingForm.controls['pin'].updateValueAndValidity();
            } else {
                this.scannerSettingForm.controls['pin'].setValue('');
                this.scannerSettingForm.controls['pin'].setValidators([]);
                this.scannerSettingForm.controls['pin'].updateValueAndValidity();
            }
        }
    }
}



