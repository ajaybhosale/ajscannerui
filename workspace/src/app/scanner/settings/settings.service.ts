import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { Settings } from '../settings/settings';
import { environment } from '../../../environments/environment';
import { Helpers } from '../../helpers';

@Injectable()
export class SettingsService {
    private parentURL: string = environment.apiUrl;
    private scanSettingsURL = 'scanners';
    private limit = 'limit=10';
    private data: Settings;

    constructor(private _http: HttpClient, private router: Router) { }

    addForm(formData: Settings): Observable<any> {
        return this._http.post(this.parentURL + this.scanSettingsURL, formData).catch(this.handleError).finally(() => {
        });
    }

    updateGroup(formData: Settings, id: string): Observable<any> {
        Helpers.setLoading(true);
        return this._http.put(this.parentURL + this.scanSettingsURL + "/" + id, formData).catch(this.handleError).finally(() => {
            Helpers.setLoading(false);
        });
    }

    getSettingsList(merchantId: string) {
        return this._http.get<Settings>(this.parentURL + this.scanSettingsURL + "/getAllScannerSettings?merchantId=" + merchantId).catch(this.handleError).finally(() => {
        });
    }

    deleteData(id: string) {
        Helpers.setLoading(true);
        return this._http.delete(this.parentURL + this.scanSettingsURL + "/" + id).catch(this.handleError).finally(() => {
            Helpers.setLoading(false);
        });
    }

    setEditFromData(formData: Settings): void {
        this.data = formData;
    }

    getEditFromData(): Settings {
        return this.data;
    }

    private handleError(error: Response) {
        return Observable.throw(error);
    }

}