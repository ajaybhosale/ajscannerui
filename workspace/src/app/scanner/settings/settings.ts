export class Settings {
    id: string;
    merchant_id: string;
    serial_no: string;
    ip: string;
    port: string;
    source_key: string;
    transaction_gateway: string;
    created_by: string;
    updated_by: string;
    type: string;
    pin: string;
    check_status: string;
}