import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { SettingsService } from '../settings/settings.service';
import { Settings } from '../settings/settings';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Helpers } from '../../helpers';
import { CookieService } from 'ngx-cookie-service';
import { AlertService, AlertMessage } from '../../auth/_services/alert.service';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
declare var jQuery: any;

@Component({
    selector: "app-setting",
    templateUrl: "./settings.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class SettingsComponent implements OnInit {
    scanDataForm: FormGroup[] = [];
    pageNumber: number = 1;
    settingList: Settings[] = [];
    currentRecord: number = 0;
    settingDetails: Settings;
    noOfRecords: number;
    isSuccess: boolean;
    isError: boolean;
    responseMsg: string;
    messageOnModal: string;
    groupName: string;
    messageSuccess: boolean;
    totalItems: number;
    errors: any;
    recordId: string;
    recordindex: number;
    showAddScanner: boolean;
    transactionGateways: any = JSON.parse(environment.transactionGateway);
    beforeDisplay: boolean;
    private showErrorCode: boolean = environment.showErrorCode;
    private fieldCode: any = {
        payee_name: "SC001",
        payor_name: "SC002",
        date: "SC003",
        sec_code: "SC004",
        amount: "SC005",
        account_number: "SC006",
        routing_number: "SC007",
        status: "SC008"
    };

    @ViewChild('confirmModal') confirmModal: ElementRef;

    constructor(
        private settingService: SettingsService,
        private formBuilder: FormBuilder,
        private CookieService: CookieService,
        private alertService: AlertService,
        private titleService: Title,
        private router: Router
    ) {

    }

    ngOnInit() {
        this.beforeDisplay = false;
        this.getSettingsList();
        this.hideErrors();
        this.showAddScanner = false;
        this.titleService.setTitle(Helpers.getBrowserTitle() + " | Scanner Settings");
    }

    hideErrors() {
        this.errors = '';
        this.isError = false;
        this.isSuccess = false;
    }

    getResponse(response: any) {
    }

    getSettingsList() {
        Helpers.setLoading(true);
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;

        this.settingService.getSettingsList(merchantId)
            .subscribe(reponse => {
                this.beforeDisplay = true;
                this.settingList = reponse;
                this.showAddScanner = (this.settingList.length) ? false : true;
                Helpers.setLoading(false);
            }, error => {
                this.beforeDisplay = true;
                this.noOfRecords = 0;
                this.totalItems = 0;
                this.isError = true;
                this.showAddScanner = false;
                this.responseMsg = 'Unable to process your request, Please contact Administrator.';
                Helpers.setLoading(false);
            });
    }

    pageChange(pageNumber) {
        this.currentRecord = (pageNumber - 1);
        this.pageNumber = pageNumber;
    }

    confirmDelete(deleteId: string, index: number) {
        this.hideErrors();
        this.recordId = deleteId;
        this.recordindex = index;
        jQuery(this.confirmModal.nativeElement).modal('show');
    }

    deleteData() {
        jQuery(this.confirmModal.nativeElement).modal('hide');
        Helpers.setLoading(true);

        this.settingService.deleteData(this.recordId).subscribe(response => {
            //this.ngOnInit();           
            this.settingList.splice(this.recordindex, 1);
            this.showAddScanner = (this.settingList.length) ? false : true;
            sessionStorage.removeItem("scannerSetting");
            sessionStorage.removeItem("scannerUrl");
            this.isSuccess = true;
            this.responseMsg = "Record successfully deleted.";
            Helpers.setLoading(false);
        }, error => {
            this.isError = true;
            this.showAddScanner = false;
            this.settingList = this.settingList;
            this.showAddScanner = (this.settingList.length) ? false : true;
            this.responseMsg = 'Unable to process your request, Please contact Administrator.';
            Helpers.setLoading(false);
        });
    }

    editForm(id: string) {
        /*this.settingService.setEditFromData(formData);*/
        this.router.navigate(['/scanner/settings/form/:' + id]);
    }

    onCancel() {
        this.hideErrors();
        this.router.navigate(['/scanner/scan-list']);
    }
}



