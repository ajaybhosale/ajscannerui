import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Helpers } from '../../helpers';

@Injectable()
export class ResultService {
    private parentURL: string = environment.apiUrl;
    private scanData = 'transaction';

    constructor(private _http: HttpClient, private router: Router) { }

    getTransactionDetails(merchantId: string, transactionGroupId: string) {
        return this._http.get(this.parentURL + this.scanData + "/getTransactionDetails?merchantId=" + merchantId + "&transactionGroupId=" + transactionGroupId).catch(this.handleError).finally(() => {
        });
    }

    private handleError(error: Response) {
        return Observable.throw(error);
    }

}