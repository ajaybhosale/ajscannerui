import { Component, OnInit, ViewEncapsulation } from '@angular/core'
import { Title } from '@angular/platform-browser';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helpers } from '../../helpers';
import { CookieService } from 'ngx-cookie-service';
import { ResultService } from '../result/result.service';
import { CurrencyPipe, DatePipe } from '@angular/common'
import { environment } from '../../../environments/environment';

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
declare var jQuery: any;

@Component({
    selector: "app-result",
    templateUrl: "./result.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class ResultComponent implements OnInit {
    resultList: any[] = [];
    transactionGroupId: string;
    errorList: any = [];
    cntTransactionFailed: number;
    beforeDisplay: boolean;
    sortBy: string = 'updated_on';
    sortOrderBy: string = 'desc';
    constructor(
        private CookieService: CookieService,
        private router: Router,
        private resultService: ResultService,
        private datePipe: DatePipe,
        private titleService: Title
    ) {

    }

    ngOnInit() {
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        let merchantId = currentUserObj.oid;
        this.transactionGroupId = null;
        this.cntTransactionFailed = 0;
        this.beforeDisplay = false;
        this.titleService.setTitle(Helpers.getBrowserTitle() + " | Result");

        if (sessionStorage.getItem('transactionData') != null) {
            this.beforeDisplay = true;
            let transactionData = JSON.parse(atob(sessionStorage.getItem('transactionData')));
            sessionStorage.removeItem('transactionData');
            this.resultList = transactionData;
            this.processResult();
            Helpers.setLoading(false);
        } else {
            Helpers.setLoading(true);
            this.transactionGroupId = atob(sessionStorage.getItem('transactionGroupId'));
            this.resultService.getTransactionDetails(merchantId, this.transactionGroupId).subscribe(reponse => {
                this.resultList = reponse;
                this.beforeDisplay = true;
                Helpers.setLoading(false);
                this.processResult();
            }, error => {
                this.beforeDisplay = true;
                Helpers.setLoading(false);
            }
            );
        }
    }

    processResult() {
        let index = 0;
        this.resultList.filter(resultItem => {
            index++;
            resultItem.account_number = resultItem.account_number;
            resultItem.routing_number = resultItem.routing_number;
            if (resultItem.isValid) {
                this.errorList.push("");
            } else {
                this.cntTransactionFailed++;
                let errString = '';
                console.log(resultItem.validationError);
                let errMsgobj = JSON.parse(resultItem.validationError);

                let errArr = [];
                if (errMsgobj.message == false || errMsgobj.message == undefined) {
                    errString += '<li>&nbsp;&nbsp;&nbsp;<span>#' + index + '</span>&nbsp;&nbsp;&nbsp;<span>Transaction failed due to unknown reasons</span>.</li>';
                } else {
                    errArr = errMsgobj.message.split('|');
                    errArr.filter(msg => {
                        let preText = '';
                        if (msg.includes('amount')) {
                            preText = resultItem.amount + ' - ';
                        } else if (msg.includes('routing_number')) {
                            preText = resultItem.routing_number + ' - ';
                        } else if (msg.includes('account_number')) {
                            preText = resultItem.account_number + ' - ';
                        }
                        errString += '<li>&nbsp;&nbsp;&nbsp;<span>#' + index + '</span>&nbsp;&nbsp;&nbsp;<span>' + preText + msg + '</span></li>';

                    });
                }
                this.errorList.push(errString);
            }
        });
    }
}