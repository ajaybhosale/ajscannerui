import { Component, OnInit, ViewEncapsulation, ElementRef, ViewChild } from '@angular/core'
import { Title } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Helpers } from '../../helpers';
import { CookieService } from 'ngx-cookie-service';
import { ReportService } from '../report/report.service';
import { environment } from '../../../environments/environment';
import { Settings } from '../settings/settings';
import { Observable } from 'rxjs/Rx';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IMyDpOptions, MyDatePickerModule } from 'mydatepicker';

declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
declare var jQuery: any;

@Component({
    selector: "app-report",
    templateUrl: "./report.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class ReportComponent implements OnInit {
    resultList: any[] = [];
    searchForm: FormGroup;
    showDefault: boolean;
    showSecCode: boolean;
    showTransactionStatus: boolean;
    merchantId: string;
    displayRecords: boolean = false;
    datePlaceHolder: string = environment.dateFormat.toLowerCase();
    endDate: any;
    startDateValidationMsg: any;
    endDateValidationMsg: any;
    isFormSubmit: boolean = false;
    sortBy: string = 'updated_on';
    sortOrderBy: string = 'desc';
    @ViewChild('btnSearch') btnSearch: ElementRef;


    startDateOption: IMyDpOptions = {
        dateFormat: environment.dateFormat.toLowerCase(),
        indicateInvalidDate: true,
        openSelectorTopOfInput: false,
        selectorHeight: '264px',
        selectorWidth: '264px',
        showSelectorArrow: false
    };

    endDateOption: IMyDpOptions = {
        dateFormat: environment.dateFormat.toLowerCase(),
        indicateInvalidDate: true,
        openSelectorTopOfInput: false,
        selectorHeight: '264px',
        selectorWidth: '264px',
        showSelectorArrow: false
    };

    constructor(
        private formBuilder: FormBuilder,
        private CookieService: CookieService,
        private router: Router,
        private datePipe: DatePipe,
        private reportService: ReportService,
        private titleService: Title
    ) {

    }

    ngOnInit() {
        let token = this.CookieService.get('token');
        let currentUserObj = Helpers.decodeToken(token);
        this.merchantId = currentUserObj.oid;
        this.displayRecords = false;
        this.isFormSubmit = false;
        this.titleService.setTitle(Helpers.getBrowserTitle() + " | Report");
        //var date_regex = /^((0|1)\d{1})-((0|1|2)\d{1})-((19|20)\d{2})/g ;

        this.changeElement(undefined);
        let beginDate = new Date(new Date().getTime() - (7 * 24 * 60 * 60 * 1000));
        let endDate = new Date();

        let fromDate = { date: { year: beginDate.getFullYear(), month: (beginDate.getMonth() + 1), day: beginDate.getDate() } };
        let toDate = { date: { year: endDate.getFullYear(), month: (endDate.getMonth() + 1), day: endDate.getDate() } };


        this.searchForm = this.formBuilder.group({
            filter_on: [""],
            from_date: [fromDate, [Validators.required,/*Validators.pattern(date_regex)*/ Validators.maxLength(15), Helpers.endDateValidator]],
            to_date: [toDate, [Validators.required,/*Validators.pattern(date_regex)*/ Validators.maxLength(15), Helpers.endDateValidator]],
            value: [""]
        });

        this.disableDate(new Date(beginDate.getFullYear(), beginDate.getMonth(), beginDate.getDate()));
        let rptStartDate = this.datePipe.transform(new Date(beginDate.getFullYear(), beginDate.getMonth(), beginDate.getDate()), environment.dateFormat);
        let rptEndDate = this.datePipe.transform(new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()), environment.dateFormat);
        let data = JSON.parse('{"merchantId":"' + this.merchantId + '","fromDate":"' + rptStartDate + '","toDate":"' + rptEndDate + '"}');
        this.reportData(data);
    }

    changeElement(data) {
        let value: string = (data != undefined) ? data.target.value : "";
        if (data != undefined && this.searchForm.controls['value'] != undefined) {
            this.searchForm.controls['value'].setValue('');
        }

        if (value.includes('secCode')) {
            this.showDefault = false;
            this.showSecCode = true;
            this.showTransactionStatus = false;
        } else if (value.includes('transactionStatus')) {
            this.showDefault = false;
            this.showSecCode = false;
            this.showTransactionStatus = true;
        } else {
            this.showDefault = true;
            this.showSecCode = false;
            this.showTransactionStatus = false;
        }
    }

    search(searchObj, valid) {
        this.isFormSubmit = true;
        if (valid) {
            Helpers.setLoading(true);
            let fromDate = "";
            let toDate = "";
            if (searchObj.from_date != null) {
                let beginDate = searchObj.from_date.date;
                fromDate = this.datePipe.transform(new Date(beginDate.year, (beginDate.month - 1), beginDate.day), environment.dateFormat);
            }
            if (searchObj.to_date != null) {
                let endDate = searchObj.to_date.date;
                toDate = this.datePipe.transform(new Date(endDate.year, (endDate.month - 1), endDate.day), environment.dateFormat);
            }
            let data = JSON.parse('{"merchantId":"' + this.merchantId + '","' + searchObj.filter_on + '":"' + searchObj.value + '","fromDate":"' + fromDate + '","toDate":"' + toDate + '"}');
            this.reportData(data);
        }
    }

    reportData(data) {

        Helpers.setLoading(true);
        this.reportService.getReviewCheckList(data).subscribe(reponse => {
            this.resultList = [];
            this.resultList = reponse;
            this.resultList.filter(data => {
                data.account_number = data.account_number;
                data.routing_number = data.routing_number;
                data.amount = parseFloat(data.amount);
                //data.routing_number = Number(data.routing_number);
                //data.account_number = Number(data.account_number);
                data.check_number = Number(data.check_number);
            });
            this.resultList = this.resultList;
            this.displayRecords = true;
            Helpers.setLoading(false);
        },
            error => {
                this.resultList = [];
                Helpers.setLoading(false);
            });

    }

    onStartDateChanged(event) {
        if (event.jsdate != null) {
            let dt: Date = new Date(event.jsdate.getTime());
            dt.setDate(dt.getDate() - 1);
            this.disableDate(dt);
        }
    }

    disableDate(dt) {
        let copy: IMyDpOptions = this.getCopyOfEndDateOptions();
        copy.disableUntil = {
            year: dt.getFullYear(),
            month: dt.getMonth() + 1,
            day: dt.getDate()
        };
        this.endDateOption = copy;
    }

    getCopyOfEndDateOptions(): IMyDpOptions {
        return JSON.parse(JSON.stringify(this.endDateOption));
    }

    startDateValidate(event) {
        var date_regex = /^((0|1)\d{1})-((0|1|2)\d{1})-((19|20)\d{2})/g;
        this.startDateValidationMsg = (event.value != '' && !event.valid) ? "Invalid date format" : "Start Date is required";
        this.searchForm.controls['to_date'].updateValueAndValidity();
    }

    endDateValidate(event) {
        var date_regex = /^((0|1)\d{1})-((0|1|2)\d{1})-((19|20)\d{2})/g;
        this.endDateValidationMsg = (event.value != '' && !event.valid) ? "Invalid date format" : "End Date is required";
        //this.endDateValidationMsg = (date_regex.test(event.value) && !event.valid)?"End Date should greater than or equal to start date":this.endDateValidationMsg;
        this.searchForm.controls['from_date'].updateValueAndValidity();
    }

    triggerSearchClick() {
        let el: HTMLElement = this.btnSearch.nativeElement as HTMLElement;
        el.click();
    }

}
