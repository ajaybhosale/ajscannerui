import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Helpers } from '../../helpers';

@Injectable()
export class ReportService {
    private parentURL: string = environment.apiUrl;
    private scanData = 'reports';

    constructor(private _http: HttpClient, private router: Router) { }

    getReviewCheckList(searchObj) {
        return this._http.post(this.parentURL + this.scanData + "/getTransactionReport", searchObj).catch(this.handleError).finally(() => {
        });
    }

    private handleError(error: Response) {
        return Observable.throw(error);
    }

}