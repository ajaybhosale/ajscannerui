import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScannerComponent } from './scanner.component';
import { ScanDataComponent } from './scan-data/scan-data.component';
import { ResultComponent } from './result/result.component';
import { SettingsComponent } from './settings/settings.component';
import { ReviewComponent } from './review/review.component';
import { ReportComponent } from './report/report.component';
import { FormSettingComponent } from './settings/form/form-setting.component';
import { AuthGuard } from "../auth/_guards/auth.guard";
import { PageComponent } from './404-page/page.component';


const routes: Routes = [
    {
        path: 'scanner',
        component: ScannerComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: "scan-data",
                component: ScanDataComponent
            },
            {
                path: "review",
                component: ReviewComponent
            },
            {
                path: "result",
                component: ResultComponent
            },
            {
                path: "settings",
                component: SettingsComponent
            },
            {
                path: "settings/form",
                component: FormSettingComponent
            },
            {
                path: "settings/form/edit",
                component: FormSettingComponent
            },
            {
                path: "report",
                component: ReportComponent
            }
        ]
    },
    {
        path: 'scanner',
        component: ScannerComponent,
        children: [{
            path: "404-page",
            component: PageComponent
        }]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ScannerRoutingModule { }