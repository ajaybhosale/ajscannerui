import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { AuthenticationService } from "../../auth/_services/authentication.service";
import { Helpers } from '../../helpers';

@Component({
    selector: "page-component",
    templateUrl: "./page.component.html",
    encapsulation: ViewEncapsulation.None,
})

export class PageComponent implements OnInit {

    constructor(private _authService: AuthenticationService) {
    }

    ngOnInit() {
        Helpers.setLoading(false);
        this._authService.logout();
    }
}