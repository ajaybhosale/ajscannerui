// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    phnxApi: 'https://backend-api-sit.azurewebsites.net/',
    apiUrl: 'http://localhost:62761/',
    redirectUri: 'https://www.scanner-ui.com:3333/oauth',
    signInUrl: 'https://www.scanner-ui.com:3333/',
    transactionGateway: '[{"id":"PHN","item":"Phoenix","enable":true},{"id":"USA","item":"USAePay","enable":true},{"id":"VCI","item":"VCI Gateway","enable":true}]',
    scannerProtocol: 'https://',
    showErrorCode: false,
    scanStatusEdit: 'edit',
    scanStatusScanned: 'scan',
    dateFormat: 'MM-dd-yyyy',
    timeFormat: 'hh:mm:ss',
    gatewayPinUSA: 'USA'
};