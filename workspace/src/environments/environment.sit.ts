export const environment = {
    production: false,
    phnxApi: 'https://backend-api-sit.azurewebsites.net/',
    //apiUrl: 'https://phnxcheckscanner.azurewebsites.net/',        
    apiUrl: 'https://phnx-scanner-sit-dit.azurewebsites.net/',
    redirectUri: 'https://phnx-scanner-sit.azurewebsites.net/oauth',
    signInUrl: 'https://phnx-scanner-sit.azurewebsites.net/',
    transactionGateway: '[{"id":"PHN","item":"Phoenix","enable":true},{"id":"USA","item":"USAePay","enable":true},{"id":"VCI","item":"VCI Gateway","enable":true}]',
    scannerProtocol: 'https://',
    showErrorCode: false,
    scanStatusEdit: 'edit',
    scanStatusScanned: 'scan',
    dateFormat: 'MM-dd-yyyy',
    timeFormat: 'hh:mm:ss',
    gatewayPinUSA: 'USA'
};
